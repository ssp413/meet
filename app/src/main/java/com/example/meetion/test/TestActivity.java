package com.example.meetion.test;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.framework.bmob.MyData;
import com.example.framework.utils.LogUtils;
import com.example.meetion.R;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;


/**
 * 测试专用类
 */
public class TestActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_add;
    private Button btn_del;
    private Button btn_query;
    private Button btn_update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        initView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add:
                MyData myData = new MyData();
                myData.setName("孙鑫鑫");
                myData.setSex(19);
                // 927de24e44
                myData.save(new SaveListener<String>() {
                    @Override
                    public void done(String s, BmobException e) {
                        if (e == null) {
                            LogUtils.i("新增成功：" + s);
                        }
                    }
                });
                break;
            case R.id.btn_del:
                break;
            case R.id.btn_query:
                BmobQuery<MyData> bmobQuery = new BmobQuery<>();
//                bmobQuery.getObject("927de24e44", new QueryListener<MyData>() {
//                    @Override
//                    public void done(MyData myData, BmobException e) {
//                        if (e == null) {
//                            LogUtils.i("mydata" + myData.getName());
//                        }else {
//                            LogUtils.e("mydata" + e.toString());
//                        }
//                    }
//                });
                bmobQuery.addWhereEqualTo("name","孙鑫鑫");
                bmobQuery.findObjects(new FindListener<MyData>() {
                    @Override
                    public void done(List<MyData> list, BmobException e) {
                        if (e == null) {
                            if (list != null && list.size() > 0) {
                                for (MyData data : list) {
                                    String name = data.getName();
                                    LogUtils.i("name" + name);
                                }
                            }
                        }
                    }
                });
                break;
            case R.id.btn_update:
                MyData myData1 = new MyData();
                myData1.setName("老鑫鑫");
                myData1.update("0c6f8eb9ab", new UpdateListener() {
                    @Override
                    public void done(BmobException e) {
                        if (e == null) {
                            Toast.makeText(TestActivity.this, "修改成功", Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(TestActivity.this, "修改失败", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;
        }
    }

    private void initView() {
        btn_add = (Button) findViewById(R.id.btn_add);
        btn_del = (Button) findViewById(R.id.btn_del);
        btn_query = (Button) findViewById(R.id.btn_query);
        btn_update = (Button) findViewById(R.id.btn_update);
        btn_add.setOnClickListener(this);
        btn_del.setOnClickListener(this);
        btn_query.setOnClickListener(this);
        btn_update.setOnClickListener(this);
    }
}