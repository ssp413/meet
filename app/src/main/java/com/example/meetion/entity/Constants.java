package com.example.meetion.entity;

public class Constants {

    // 是否第一次进入app
    public static final String SP_IS_FIRST_APP = "is_First_App";
    //Token
    public static final String SP_TOKEN = "token";
    // 手机号码
    public static final String SP_PHONE = "phone";
}
