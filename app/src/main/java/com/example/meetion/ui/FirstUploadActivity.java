package com.example.meetion.ui;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.framework.base.BaseBackActivity;
import com.example.framework.bmob.BmobManager;
import com.example.framework.helper.FileHelper;
import com.example.framework.manager.DialogManager;
import com.example.framework.utils.LogUtils;
import com.example.framework.view.DialogView;
import com.example.framework.view.LodingView;
import com.example.meetion.R;

import java.io.File;

import cn.bmob.v3.exception.BmobException;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * 头像上传activity
 */
public class FirstUploadActivity extends BaseBackActivity implements View.OnClickListener {

    private TextView tv_camera;
    private TextView tv_ablum;
    private TextView tv_cancel;

    private LodingView mLodingView;
    private DialogView mPhotoSelectView;


    /**
     * 跳转
     *
     * @param mActivity
     * @param requestCode
     */
    public static void startActivity(Activity mActivity, int requestCode) {
        Intent intent = new Intent(mActivity, FirstUploadActivity.class);
        mActivity.startActivityForResult(intent, requestCode);
    }

    private CircleImageView iv_Photo;
    private EditText et_nickname;
    private Button btn_upload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_upload);
        initView();
    }

    /**
     * 初始化view
     */
    private void initView() {

        initPhotoView();

        iv_Photo = (CircleImageView) findViewById(R.id.iv_photo);
        et_nickname = (EditText) findViewById(R.id.et_nickname);
        btn_upload = (Button) findViewById(R.id.btn_upload);

        iv_Photo.setOnClickListener(this);
        btn_upload.setOnClickListener(this);

        btn_upload.setEnabled(false);
        et_nickname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    btn_upload.setEnabled(uploadFile != null);
                } else {
                    btn_upload.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    /**
     * 初始化选择框
     */
    private void initPhotoView() {

        mLodingView = new LodingView(this);
        mLodingView.setLodingText("正在上传头像...");

        mPhotoSelectView = DialogManager.getInstance()
                .initView(this, R.layout.dialog_select_photo, Gravity.BOTTOM);
        tv_camera = mPhotoSelectView.findViewById(R.id.tv_camera);
        tv_ablum = mPhotoSelectView.findViewById(R.id.tv_ablum);
        tv_cancel = mPhotoSelectView.findViewById(R.id.tv_cancel);
        tv_camera.setOnClickListener(this);
        tv_ablum.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_camera:
                DialogManager.getInstance().hide(mPhotoSelectView);
                // 跳转到相机
                FileHelper.getInstance().toCamera(this);
                break;
            case R.id.tv_ablum:
                DialogManager.getInstance().hide(mPhotoSelectView);
                // 跳转到相册
                FileHelper.getInstance().toAlbum(this);
                break;
            case R.id.tv_cancel:
                DialogManager.getInstance().hide(mPhotoSelectView);
                break;
            case R.id.iv_photo:
                // 显示选择提示框
                DialogManager.getInstance().show(mPhotoSelectView);
                break;
            case R.id.btn_upload:
                uploadPhoto();
                break;
        }
    }

    /**
     * 上传头像
     */
    private void uploadPhoto() {
        // 如果条件没有满足 走不到这里
        String nickname = et_nickname.getText().toString();
        mLodingView.show();
        BmobManager.getInstance().uploadFirstPhoto(nickname, uploadFile, new BmobManager.OnUploadPhotoListener() {
            @Override
            public void OnUpdateDone() {
                mLodingView.hide();
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void OnUpdateFail(BmobException e) {
                mLodingView.hide();
                Toast.makeText(FirstUploadActivity.this, ""+e.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private File uploadFile = null;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        LogUtils.i("requestCode:" + requestCode);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == FileHelper.CAMEAR_REQUEST_CODE) {
                uploadFile = FileHelper.getInstance().getTempFile();
            } else if (requestCode == FileHelper.ALBUM_REQUEST_CODE) {
                assert data != null;
                Uri uri = data.getData();
                if (uri != null) {
                    // 获取真实地址
                    String path = FileHelper.getInstance().getRealPathFromURI(this, uri);
                    if (!TextUtils.isEmpty(path)) {
                        uploadFile = new File(path);
                    }
                }
            }
            // 设置头像
            if (uploadFile != null) {
                Bitmap mBitmap = BitmapFactory.decodeFile(uploadFile.getPath());
                iv_Photo.setImageBitmap(mBitmap);

                // 判断当前的输入框
                String nickname = et_nickname.getText().toString();
                btn_upload.setEnabled(!TextUtils.isEmpty(nickname));

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}