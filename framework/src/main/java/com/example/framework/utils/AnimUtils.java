package com.example.framework.utils;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.LinearInterpolator;

/**
 * 旋转动画工具类
 */
public class AnimUtils {

    public static ObjectAnimator rotation(View view) {
        // 旋转动画
        ObjectAnimator mAnim = ObjectAnimator.ofFloat(view,"rotation",0f,360f);
        // 播放时间
        mAnim.setDuration(2 * 1000);
        // 循环播放
        mAnim.setRepeatMode(ValueAnimator.RESTART);
        // 一直播放
        mAnim.setRepeatCount(ValueAnimator.INFINITE);
        mAnim.setInterpolator(new LinearInterpolator());
        return mAnim;
    }
}
