package com.example.framework.bmob;

import android.content.Context;
import android.widget.Toast;

import java.io.File;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobSMS;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.LogInListener;
import cn.bmob.v3.listener.QueryListener;
import cn.bmob.v3.listener.UpdateListener;
import cn.bmob.v3.listener.UploadFileListener;

/**
 * Bmob管理类
 */
public class BmobManager {

    private static final String BMOB_SDK_ID = "a51ec75a70cb7da8baa602505f3b0bc1";

    private volatile static BmobManager mInstance = null;

    private Context context;

    private BmobManager() {

    }

    public static BmobManager getInstance() {
        if (mInstance == null) {
            synchronized (BmobManager.class) {
                if (mInstance == null) {
                    mInstance = new BmobManager();
                }
            }
        }
        return mInstance;
    }

    /**
     * 初始化Bmob
     *
     * @param mContext
     */
    public void initBmob(Context mContext) {
        this.context = mContext;
        Bmob.initialize(mContext, BMOB_SDK_ID);
    }

    /**
     * 判断是否登录
     *
     * @return
     */
    public boolean isLoging() {
        return BmobUser.isLogin();
    }

    /**
     * 获取本地对象
     *
     * @return
     */
    public IMUser getUser() {
        return BmobUser.getCurrentUser(IMUser.class);
    }

    /**
     * 发送短信验证码
     *
     * @param phone    手机号
     * @param listener 回调
     */
    public void requestSMS(String phone, QueryListener<Integer> listener) {
        BmobSMS.requestSMSCode(phone, null, listener);
    }

    /**
     * 通过手机号码注册或者登录
     *
     * @param phone    手机号码
     * @param code     短信验证码
     * @param listener 回调
     */
    public void signOrLoginByMobilePhone(String phone, String code, LogInListener<IMUser> listener) {
        BmobUser.signOrLoginByMobilePhone(phone, code, listener);
    }

    /**
     * 上传头像
     * @param nickname
     * @param file
     * @param listener
     */
    public void uploadFirstPhoto(String nickname, File file, OnUploadPhotoListener listener) {
        /**
         * 1.上传文件拿到地址
         * 2.更新用户信息
         */
        final IMUser imUser = getUser();
        final BmobFile bmobFile = new BmobFile(file);
        bmobFile.uploadblock(new UploadFileListener() {
            @Override
            public void done(BmobException e) {
                if (e == null) {
                    // 上传成功
                    imUser.setNickName(nickname);
                    imUser.setPhoto(bmobFile.getFileUrl());

                    imUser.setTokenNickName(nickname);
                    imUser.setTokenPhoto(bmobFile.getFileUrl());

                    // 更新用户信息
                    imUser.update(new UpdateListener() {
                        @Override
                        public void done(BmobException e) {
                            if (e == null) {
                                listener.OnUpdateDone();
                            } else {
                                listener.OnUpdateFail(e);
                            }
                        }
                    });
                } else {
                    listener.OnUpdateFail(e);
                }
            }
        });
    }

    public interface OnUploadPhotoListener {
        void OnUpdateDone();

        void OnUpdateFail(BmobException e);
    }

}
